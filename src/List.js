import React from 'react';
import {TransitionGroup, CSSTransition} from "react-transition-group";

const List = ({items, onRemove}) => {
	return (
		<TransitionGroup component={'ul'}>
			{
				items.map(item => (
					<CSSTransition
						timeout={500}
						classNames={'orange'}
						key={item.id}
					>
						<li
							style={{cursor: 'pointer'}}
							onClick={() => onRemove(item.id)}
						>{item.title}</li>
					</CSSTransition>
				))
			}
		</TransitionGroup>
	);
};

export default List;